import java.sql.*;
import java.util.Collections;

public class Main {

    public static void main(String[] args) {

        JSONReader jsonReader = new JSONReader();

        jsonReader.readJSON("statuses.json");

        String removeBefore = "2017-07-01 00:00:00";
        Timestamp ts = jsonReader.stringToTimestamp(removeBefore);

        jsonReader.readedObject.removeOlderThan(ts);

        Collections.sort(jsonReader.readedObject.getRecords());

        jsonReader.readedObject.saveToCsv("Result");
    }
}
