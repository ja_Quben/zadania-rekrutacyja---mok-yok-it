import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class ReadedObject {
    private int next_offset;
    private List<Record> records;

    public void removeOlderThan(Timestamp ts){
        List<Record> elementsToRemove = new ArrayList<Record>();

        for(int i = 0; i < records.size(); i++){
            if(records.get(i).getKontakt_ts().before(ts)){
                elementsToRemove.add(records.get(i));
            }
        }

        records.removeAll(elementsToRemove);
    }

    public void saveToCsv(String fileName) {
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(new FileOutputStream(fileName + ".csv"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        for (Record record : records)
            pw.println(record.toString());
        pw.close();
    }

    public int getNext_offset() {
        return next_offset;
    }

    public void setNext_offset(int next_offset) {
        this.next_offset = next_offset;
    }

    public List<Record> getRecords() {
        return records;
    }

    public void setRecords(List<Record> records) {
        this.records = records;
    }
}
