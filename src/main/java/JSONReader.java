import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JSONReader {
    ReadedObject readedObject;

    public void readJSON(String path){
        Gson gson = new Gson();

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(path));
            readedObject = gson.fromJson(bufferedReader, ReadedObject.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Timestamp stringToTimestamp(String ts){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date parsedDate = null;
        try {
            parsedDate = dateFormat.parse(ts);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());

        return timestamp;
    }

}
