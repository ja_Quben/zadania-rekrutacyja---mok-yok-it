import java.sql.Timestamp;

public class Record implements Comparable<Record> {

    private int kontakt_id;
    private int klient_id;
    private int pracownik_id;
    private String status;
    private Timestamp kontakt_ts;


    public int getKontakt_id() {
        return kontakt_id;
    }

    public void setKontakt_id(int kontakt_id) {
        this.kontakt_id = kontakt_id;
    }

    public int getKlient_id() {
        return klient_id;
    }

    public void setKlient_id(int klient_id) {
        this.klient_id = klient_id;
    }

    public int getPracownik_id() {
        return pracownik_id;
    }

    public void setPracownik_id(int pracownik_id) {
        this.pracownik_id = pracownik_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Timestamp getKontakt_ts() {
        return kontakt_ts;
    }

    public void setKontakt_ts(Timestamp kontakt_ts) {
        this.kontakt_ts = kontakt_ts;
    }

    @Override
    public String toString() {
        return  kontakt_id + "|" + klient_id + "|" + pracownik_id + "|" + status + "|" + kontakt_ts;
    }

    public int compareTo(Record o) {
        int compareResult = Integer.compare(this.getKlient_id(), o.getKlient_id());
        if(compareResult == 0){
            compareResult = this.getKontakt_ts().compareTo(o.getKontakt_ts());
        }
        return compareResult;
    }

}
