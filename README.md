# Zadania rekrutacyjne - MOK YOK IT

ZADANIE 1

W klasie JSONReader zaimplementowano metody:
readJSON(String path) - metoda odczytująca zawartość pliku JSON, którego nazwa jest paramatrem funkcji.
Dane z tego pliku przechowywane są w klasie ReadedObject jako lista obiektów typu Record.

W klasie Record poza getterami i setterami zaimplementowano metody toString, w celu przygotowania informacji do zapisu w pliku .csv
oraz metodę compareTo rozszeżającą interfejs Comparable do posortowania listy zgodnie z treścią zadania.

W klasie ReadedObject zaimplementowano metodę do usuwania z listy elementów, których z datą przed 1.07.2017, oraz metodę, która zapisuje
zawartość listy do pliku .csv


ZADANIE 2 i 3

Zapytania do bazy danych znajdują się w folderze "Zapytania", w pliku .txt

